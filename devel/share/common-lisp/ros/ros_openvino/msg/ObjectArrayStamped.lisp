; Auto-generated. Do not edit!


(cl:in-package ros_openvino-msg)


;//! \htmlinclude ObjectArrayStamped.msg.html

(cl:defclass <ObjectArrayStamped> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (data
    :reader data
    :initarg :data
    :type (cl:vector ros_openvino-msg:Object)
   :initform (cl:make-array 0 :element-type 'ros_openvino-msg:Object :initial-element (cl:make-instance 'ros_openvino-msg:Object))))
)

(cl:defclass ObjectArrayStamped (<ObjectArrayStamped>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ObjectArrayStamped>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ObjectArrayStamped)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_openvino-msg:<ObjectArrayStamped> is deprecated: use ros_openvino-msg:ObjectArrayStamped instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ObjectArrayStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:header-val is deprecated.  Use ros_openvino-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <ObjectArrayStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:data-val is deprecated.  Use ros_openvino-msg:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ObjectArrayStamped>) ostream)
  "Serializes a message object of type '<ObjectArrayStamped>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'data))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ObjectArrayStamped>) istream)
  "Deserializes a message object of type '<ObjectArrayStamped>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'data) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'data)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ros_openvino-msg:Object))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ObjectArrayStamped>)))
  "Returns string type for a message object of type '<ObjectArrayStamped>"
  "ros_openvino/ObjectArrayStamped")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ObjectArrayStamped)))
  "Returns string type for a message object of type 'ObjectArrayStamped"
  "ros_openvino/ObjectArrayStamped")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ObjectArrayStamped>)))
  "Returns md5sum for a message object of type '<ObjectArrayStamped>"
  "330c05cf1463f0666552e4359e9fcb3b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ObjectArrayStamped)))
  "Returns md5sum for a message object of type 'ObjectArrayStamped"
  "330c05cf1463f0666552e4359e9fcb3b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ObjectArrayStamped>)))
  "Returns full string definition for message of type '<ObjectArrayStamped>"
  (cl:format cl:nil "########################################~%# Message for object array used to save object (include person) detection result~%#~%# Created on 2019-09-17~%########################################~%~%std_msgs/Header header~%Object[] data~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: ros_openvino/Object~%########################################~%# Message for single object detected~%#~%# Created on 2019-09-17~%########################################~%~%BBox bbox~%ObjectAttr obj_attr~%~%================================================================================~%MSG: ros_openvino/BBox~%########################################~%# Message for defining bounding box used in object detection result~%#~%# Created on 2018-12-12~%########################################~%~%string label~%uint8 cls_id~%string cls_name~%uint16 top_x~%uint16 top_y~%uint16 width~%uint16 height~%float32 prob    # OD probability~%~%================================================================================~%MSG: ros_openvino/ObjectAttr~%########################################~%# Message for non-person object attributes~%#~%# Created on 2019-09-17~%########################################~%~%string color~%float32 area~%string area_label   # small, middle, or large~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ObjectArrayStamped)))
  "Returns full string definition for message of type 'ObjectArrayStamped"
  (cl:format cl:nil "########################################~%# Message for object array used to save object (include person) detection result~%#~%# Created on 2019-09-17~%########################################~%~%std_msgs/Header header~%Object[] data~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: ros_openvino/Object~%########################################~%# Message for single object detected~%#~%# Created on 2019-09-17~%########################################~%~%BBox bbox~%ObjectAttr obj_attr~%~%================================================================================~%MSG: ros_openvino/BBox~%########################################~%# Message for defining bounding box used in object detection result~%#~%# Created on 2018-12-12~%########################################~%~%string label~%uint8 cls_id~%string cls_name~%uint16 top_x~%uint16 top_y~%uint16 width~%uint16 height~%float32 prob    # OD probability~%~%================================================================================~%MSG: ros_openvino/ObjectAttr~%########################################~%# Message for non-person object attributes~%#~%# Created on 2019-09-17~%########################################~%~%string color~%float32 area~%string area_label   # small, middle, or large~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ObjectArrayStamped>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'data) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ObjectArrayStamped>))
  "Converts a ROS message object to a list"
  (cl:list 'ObjectArrayStamped
    (cl:cons ':header (header msg))
    (cl:cons ':data (data msg))
))
