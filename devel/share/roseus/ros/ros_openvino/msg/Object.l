;; Auto-generated. Do not edit!


(when (boundp 'ros_openvino::Object)
  (if (not (find-package "ROS_OPENVINO"))
    (make-package "ROS_OPENVINO"))
  (shadow 'Object (find-package "ROS_OPENVINO")))
(unless (find-package "ROS_OPENVINO::OBJECT")
  (make-package "ROS_OPENVINO::OBJECT"))

(in-package "ROS")
;;//! \htmlinclude Object.msg.html


(defclass ros_openvino::Object
  :super ros::object
  :slots (_label _confidence _x _y _height _width ))

(defmethod ros_openvino::Object
  (:init
   (&key
    ((:label __label) "")
    ((:confidence __confidence) 0.0)
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:height __height) 0.0)
    ((:width __width) 0.0)
    )
   (send-super :init)
   (setq _label (string __label))
   (setq _confidence (float __confidence))
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _height (float __height))
   (setq _width (float __width))
   self)
  (:label
   (&optional __label)
   (if __label (setq _label __label)) _label)
  (:confidence
   (&optional __confidence)
   (if __confidence (setq _confidence __confidence)) _confidence)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:height
   (&optional __height)
   (if __height (setq _height __height)) _height)
  (:width
   (&optional __width)
   (if __width (setq _width __width)) _width)
  (:serialization-length
   ()
   (+
    ;; string _label
    4 (length _label)
    ;; float32 _confidence
    4
    ;; float32 _x
    4
    ;; float32 _y
    4
    ;; float32 _height
    4
    ;; float32 _width
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _label
       (write-long (length _label) s) (princ _label s)
     ;; float32 _confidence
       (sys::poke _confidence (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _height
       (sys::poke _height (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _width
       (sys::poke _width (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _label
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _label (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _confidence
     (setq _confidence (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _height
     (setq _height (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _width
     (setq _width (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get ros_openvino::Object :md5sum-) "e3004c93bb7bce36585d049015517be6")
(setf (get ros_openvino::Object :datatype-) "ros_openvino/Object")
(setf (get ros_openvino::Object :definition-)
      "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).
#Copyright (c) 2019 Giovanni di Dio Bruno.
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
# 
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#This message is for one 2D object detection result

#which object is detected
string label

#confidence of result
float32 confidence

#normalized value of box top-left point coordinate x
float32 x

#normalized value of box top-left point coordinate y
float32 y

#normalized value of box height
float32 height

#normalized value of box width
float32 width

")



(provide :ros_openvino/Object "e3004c93bb7bce36585d049015517be6")


