;; Auto-generated. Do not edit!


(when (boundp 'ros_openvino::BBox)
  (if (not (find-package "ROS_OPENVINO"))
    (make-package "ROS_OPENVINO"))
  (shadow 'BBox (find-package "ROS_OPENVINO")))
(unless (find-package "ROS_OPENVINO::BBOX")
  (make-package "ROS_OPENVINO::BBOX"))

(in-package "ROS")
;;//! \htmlinclude BBox.msg.html


(defclass ros_openvino::BBox
  :super ros::object
  :slots (_label _cls_id _cls_name _top_x _top_y _width _height _prob ))

(defmethod ros_openvino::BBox
  (:init
   (&key
    ((:label __label) "")
    ((:cls_id __cls_id) 0)
    ((:cls_name __cls_name) "")
    ((:top_x __top_x) 0)
    ((:top_y __top_y) 0)
    ((:width __width) 0)
    ((:height __height) 0)
    ((:prob __prob) 0.0)
    )
   (send-super :init)
   (setq _label (string __label))
   (setq _cls_id (round __cls_id))
   (setq _cls_name (string __cls_name))
   (setq _top_x (round __top_x))
   (setq _top_y (round __top_y))
   (setq _width (round __width))
   (setq _height (round __height))
   (setq _prob (float __prob))
   self)
  (:label
   (&optional __label)
   (if __label (setq _label __label)) _label)
  (:cls_id
   (&optional __cls_id)
   (if __cls_id (setq _cls_id __cls_id)) _cls_id)
  (:cls_name
   (&optional __cls_name)
   (if __cls_name (setq _cls_name __cls_name)) _cls_name)
  (:top_x
   (&optional __top_x)
   (if __top_x (setq _top_x __top_x)) _top_x)
  (:top_y
   (&optional __top_y)
   (if __top_y (setq _top_y __top_y)) _top_y)
  (:width
   (&optional __width)
   (if __width (setq _width __width)) _width)
  (:height
   (&optional __height)
   (if __height (setq _height __height)) _height)
  (:prob
   (&optional __prob)
   (if __prob (setq _prob __prob)) _prob)
  (:serialization-length
   ()
   (+
    ;; string _label
    4 (length _label)
    ;; uint8 _cls_id
    1
    ;; string _cls_name
    4 (length _cls_name)
    ;; uint16 _top_x
    2
    ;; uint16 _top_y
    2
    ;; uint16 _width
    2
    ;; uint16 _height
    2
    ;; float32 _prob
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _label
       (write-long (length _label) s) (princ _label s)
     ;; uint8 _cls_id
       (write-byte _cls_id s)
     ;; string _cls_name
       (write-long (length _cls_name) s) (princ _cls_name s)
     ;; uint16 _top_x
       (write-word _top_x s)
     ;; uint16 _top_y
       (write-word _top_y s)
     ;; uint16 _width
       (write-word _width s)
     ;; uint16 _height
       (write-word _height s)
     ;; float32 _prob
       (sys::poke _prob (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _label
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _label (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint8 _cls_id
     (setq _cls_id (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; string _cls_name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _cls_name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint16 _top_x
     (setq _top_x (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _top_y
     (setq _top_y (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _width
     (setq _width (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _height
     (setq _height (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; float32 _prob
     (setq _prob (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get ros_openvino::BBox :md5sum-) "ca04bfc116938fe901ae8d0c896cf4ab")
(setf (get ros_openvino::BBox :datatype-) "ros_openvino/BBox")
(setf (get ros_openvino::BBox :definition-)
      "########################################
# Message for defining bounding box used in object detection result
#
# Created on 2018-12-12
########################################

string label
uint8 cls_id
string cls_name
uint16 top_x
uint16 top_y
uint16 width
uint16 height
float32 prob    # OD probability

")



(provide :ros_openvino/BBox "ca04bfc116938fe901ae8d0c896cf4ab")


