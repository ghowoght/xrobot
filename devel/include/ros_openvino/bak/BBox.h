// Generated by gencpp from file ros_openvino/BBox.msg
// DO NOT EDIT!


#ifndef ROS_OPENVINO_MESSAGE_BBOX_H
#define ROS_OPENVINO_MESSAGE_BBOX_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace ros_openvino
{
template <class ContainerAllocator>
struct BBox_
{
  typedef BBox_<ContainerAllocator> Type;

  BBox_()
    : label()
    , cls_id(0)
    , cls_name()
    , top_x(0)
    , top_y(0)
    , width(0)
    , height(0)
    , prob(0.0)  {
    }
  BBox_(const ContainerAllocator& _alloc)
    : label(_alloc)
    , cls_id(0)
    , cls_name(_alloc)
    , top_x(0)
    , top_y(0)
    , width(0)
    , height(0)
    , prob(0.0)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _label_type;
  _label_type label;

   typedef uint8_t _cls_id_type;
  _cls_id_type cls_id;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _cls_name_type;
  _cls_name_type cls_name;

   typedef uint16_t _top_x_type;
  _top_x_type top_x;

   typedef uint16_t _top_y_type;
  _top_y_type top_y;

   typedef uint16_t _width_type;
  _width_type width;

   typedef uint16_t _height_type;
  _height_type height;

   typedef float _prob_type;
  _prob_type prob;





  typedef boost::shared_ptr< ::ros_openvino::BBox_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::ros_openvino::BBox_<ContainerAllocator> const> ConstPtr;

}; // struct BBox_

typedef ::ros_openvino::BBox_<std::allocator<void> > BBox;

typedef boost::shared_ptr< ::ros_openvino::BBox > BBoxPtr;
typedef boost::shared_ptr< ::ros_openvino::BBox const> BBoxConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::ros_openvino::BBox_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::ros_openvino::BBox_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::ros_openvino::BBox_<ContainerAllocator1> & lhs, const ::ros_openvino::BBox_<ContainerAllocator2> & rhs)
{
  return lhs.label == rhs.label &&
    lhs.cls_id == rhs.cls_id &&
    lhs.cls_name == rhs.cls_name &&
    lhs.top_x == rhs.top_x &&
    lhs.top_y == rhs.top_y &&
    lhs.width == rhs.width &&
    lhs.height == rhs.height &&
    lhs.prob == rhs.prob;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::ros_openvino::BBox_<ContainerAllocator1> & lhs, const ::ros_openvino::BBox_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace ros_openvino

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::ros_openvino::BBox_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::ros_openvino::BBox_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ros_openvino::BBox_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ros_openvino::BBox_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ros_openvino::BBox_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ros_openvino::BBox_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::ros_openvino::BBox_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ca04bfc116938fe901ae8d0c896cf4ab";
  }

  static const char* value(const ::ros_openvino::BBox_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xca04bfc116938fe9ULL;
  static const uint64_t static_value2 = 0x01ae8d0c896cf4abULL;
};

template<class ContainerAllocator>
struct DataType< ::ros_openvino::BBox_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ros_openvino/BBox";
  }

  static const char* value(const ::ros_openvino::BBox_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::ros_openvino::BBox_<ContainerAllocator> >
{
  static const char* value()
  {
    return "########################################\n"
"# Message for defining bounding box used in object detection result\n"
"#\n"
"# Created on 2018-12-12\n"
"########################################\n"
"\n"
"string label\n"
"uint8 cls_id\n"
"string cls_name\n"
"uint16 top_x\n"
"uint16 top_y\n"
"uint16 width\n"
"uint16 height\n"
"float32 prob    # OD probability\n"
;
  }

  static const char* value(const ::ros_openvino::BBox_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::ros_openvino::BBox_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.label);
      stream.next(m.cls_id);
      stream.next(m.cls_name);
      stream.next(m.top_x);
      stream.next(m.top_y);
      stream.next(m.width);
      stream.next(m.height);
      stream.next(m.prob);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct BBox_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::ros_openvino::BBox_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::ros_openvino::BBox_<ContainerAllocator>& v)
  {
    s << indent << "label: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.label);
    s << indent << "cls_id: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.cls_id);
    s << indent << "cls_name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.cls_name);
    s << indent << "top_x: ";
    Printer<uint16_t>::stream(s, indent + "  ", v.top_x);
    s << indent << "top_y: ";
    Printer<uint16_t>::stream(s, indent + "  ", v.top_y);
    s << indent << "width: ";
    Printer<uint16_t>::stream(s, indent + "  ", v.width);
    s << indent << "height: ";
    Printer<uint16_t>::stream(s, indent + "  ", v.height);
    s << indent << "prob: ";
    Printer<float>::stream(s, indent + "  ", v.prob);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ROS_OPENVINO_MESSAGE_BBOX_H
