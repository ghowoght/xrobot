(cl:in-package ros_openvino-msg)
(cl:export '(LABEL-VAL
          LABEL
          CONFIDENCE-VAL
          CONFIDENCE
          X-VAL
          X
          Y-VAL
          Y
          HEIGHT-VAL
          HEIGHT
          WIDTH-VAL
          WIDTH
))