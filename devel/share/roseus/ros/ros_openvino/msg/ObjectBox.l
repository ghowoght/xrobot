;; Auto-generated. Do not edit!


(when (boundp 'ros_openvino::ObjectBox)
  (if (not (find-package "ROS_OPENVINO"))
    (make-package "ROS_OPENVINO"))
  (shadow 'ObjectBox (find-package "ROS_OPENVINO")))
(unless (find-package "ROS_OPENVINO::OBJECTBOX")
  (make-package "ROS_OPENVINO::OBJECTBOX"))

(in-package "ROS")
;;//! \htmlinclude ObjectBox.msg.html


(defclass ros_openvino::ObjectBox
  :super ros::object
  :slots (_label _cls_id _confidence _x _y _z _width _height _depth ))

(defmethod ros_openvino::ObjectBox
  (:init
   (&key
    ((:label __label) "")
    ((:cls_id __cls_id) 0)
    ((:confidence __confidence) 0.0)
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:z __z) 0.0)
    ((:width __width) 0.0)
    ((:height __height) 0.0)
    ((:depth __depth) 0.0)
    )
   (send-super :init)
   (setq _label (string __label))
   (setq _cls_id (round __cls_id))
   (setq _confidence (float __confidence))
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _z (float __z))
   (setq _width (float __width))
   (setq _height (float __height))
   (setq _depth (float __depth))
   self)
  (:label
   (&optional __label)
   (if __label (setq _label __label)) _label)
  (:cls_id
   (&optional __cls_id)
   (if __cls_id (setq _cls_id __cls_id)) _cls_id)
  (:confidence
   (&optional __confidence)
   (if __confidence (setq _confidence __confidence)) _confidence)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:z
   (&optional __z)
   (if __z (setq _z __z)) _z)
  (:width
   (&optional __width)
   (if __width (setq _width __width)) _width)
  (:height
   (&optional __height)
   (if __height (setq _height __height)) _height)
  (:depth
   (&optional __depth)
   (if __depth (setq _depth __depth)) _depth)
  (:serialization-length
   ()
   (+
    ;; string _label
    4 (length _label)
    ;; uint8 _cls_id
    1
    ;; float32 _confidence
    4
    ;; float32 _x
    4
    ;; float32 _y
    4
    ;; float32 _z
    4
    ;; float32 _width
    4
    ;; float32 _height
    4
    ;; float32 _depth
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _label
       (write-long (length _label) s) (princ _label s)
     ;; uint8 _cls_id
       (write-byte _cls_id s)
     ;; float32 _confidence
       (sys::poke _confidence (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _z
       (sys::poke _z (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _width
       (sys::poke _width (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _height
       (sys::poke _height (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _depth
       (sys::poke _depth (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _label
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _label (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint8 _cls_id
     (setq _cls_id (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; float32 _confidence
     (setq _confidence (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _z
     (setq _z (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _width
     (setq _width (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _height
     (setq _height (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _depth
     (setq _depth (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get ros_openvino::ObjectBox :md5sum-) "fa99cffdca4ba0958f353c904950d0fd")
(setf (get ros_openvino::ObjectBox :datatype-) "ros_openvino/ObjectBox")
(setf (get ros_openvino::ObjectBox :definition-)
      "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).
#Copyright (c) 2019 Giovanni di Dio Bruno.
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
# 
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#This message is for one 3D object detection result

#which object is detected
string label

uint8 cls_id

#confidence of result
float32 confidence

#box center of mass -> x in meters referred to frame_id parameter
float32 x

#box center of mass -> y in meters referred to frame_id parameter
float32 y

#box center of mass -> z in meters referred to frame_id parameter
float32 z

#box size -> width in meters
float32 width

#box size -> height in meters
float32 height

#box size ->depth in meters
float32 depth
")



(provide :ros_openvino/ObjectBox "fa99cffdca4ba0958f353c904950d0fd")


