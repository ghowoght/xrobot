#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export LD_LIBRARY_PATH='/home/ghowoght/workplace/myrobot/devel/lib:/home/ghowoght/catkin_ws/devel/lib:/opt/ros/melodic/lib:/opt/intel/openvino_2020.4.287/data_processing/dl_streamer/lib:/opt/intel/openvino_2020.4.287/data_processing/gstreamer/lib:/opt/intel/openvino_2020.4.287/opencv/lib:/opt/intel/openvino_2020.4.287/deployment_tools/ngraph/lib:/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/external/hddl_unite/lib:/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/external/hddl/lib:/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/external/gna/lib:/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/external/mkltiny_lnx/lib:/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/external/tbb/lib:/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/lib/intel64'
export PATH='/opt/ros/melodic/bin:/home/ghowoght/.local/bin:/usr/lib/jvm/jdk-11.0.8/bin:/opt/intel/openvino_2020.4.287/deployment_tools/model_optimizer:/opt/intel/openvino_2020.4.287/data_processing/gstreamer/bin:/opt/intel/openvino_2020.4.287/data_processing/gstreamer/bin/gstreamer-1.0:/home/ghowoght/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin'
export PKG_CONFIG_PATH='/home/ghowoght/workplace/myrobot/devel/lib/pkgconfig:/home/ghowoght/catkin_ws/devel/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig:/opt/intel/openvino_2020.4.287/data_processing/dl_streamer/lib/pkgconfig:/opt/intel/openvino_2020.4.287/data_processing/gstreamer/lib/pkgconfig'
export PWD='/home/ghowoght/workplace/myrobot/build'
export PYTHONPATH='/home/ghowoght/workplace/myrobot/devel/lib/python2.7/dist-packages:/home/ghowoght/catkin_ws/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/opt/intel/openvino_2020.4.287/python/python3.6:/opt/intel/openvino_2020.4.287/python/python3:/opt/intel/openvino_2020.4.287/deployment_tools/open_model_zoo/tools/accuracy_checker:/opt/intel/openvino_2020.4.287/deployment_tools/model_optimizer:/opt/intel/openvino_2020.4.287/data_processing/dl_streamer/python:/opt/intel/openvino_2020.4.287/data_processing/gstreamer/lib/python3.6/site-packages'