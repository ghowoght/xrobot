
(cl:in-package :asdf)

(defsystem "ros_openvino-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Object" :depends-on ("_package_Object"))
    (:file "_package_Object" :depends-on ("_package"))
    (:file "ObjectBox" :depends-on ("_package_ObjectBox"))
    (:file "_package_ObjectBox" :depends-on ("_package"))
    (:file "ObjectBoxList" :depends-on ("_package_ObjectBoxList"))
    (:file "_package_ObjectBoxList" :depends-on ("_package"))
    (:file "Objects" :depends-on ("_package_Objects"))
    (:file "_package_Objects" :depends-on ("_package"))
  ))