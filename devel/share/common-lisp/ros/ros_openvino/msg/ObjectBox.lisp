; Auto-generated. Do not edit!


(cl:in-package ros_openvino-msg)


;//! \htmlinclude ObjectBox.msg.html

(cl:defclass <ObjectBox> (roslisp-msg-protocol:ros-message)
  ((label
    :reader label
    :initarg :label
    :type cl:string
    :initform "")
   (cls_id
    :reader cls_id
    :initarg :cls_id
    :type cl:fixnum
    :initform 0)
   (confidence
    :reader confidence
    :initarg :confidence
    :type cl:float
    :initform 0.0)
   (x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0)
   (z
    :reader z
    :initarg :z
    :type cl:float
    :initform 0.0)
   (width
    :reader width
    :initarg :width
    :type cl:float
    :initform 0.0)
   (height
    :reader height
    :initarg :height
    :type cl:float
    :initform 0.0)
   (depth
    :reader depth
    :initarg :depth
    :type cl:float
    :initform 0.0))
)

(cl:defclass ObjectBox (<ObjectBox>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ObjectBox>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ObjectBox)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_openvino-msg:<ObjectBox> is deprecated: use ros_openvino-msg:ObjectBox instead.")))

(cl:ensure-generic-function 'label-val :lambda-list '(m))
(cl:defmethod label-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:label-val is deprecated.  Use ros_openvino-msg:label instead.")
  (label m))

(cl:ensure-generic-function 'cls_id-val :lambda-list '(m))
(cl:defmethod cls_id-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:cls_id-val is deprecated.  Use ros_openvino-msg:cls_id instead.")
  (cls_id m))

(cl:ensure-generic-function 'confidence-val :lambda-list '(m))
(cl:defmethod confidence-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:confidence-val is deprecated.  Use ros_openvino-msg:confidence instead.")
  (confidence m))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:x-val is deprecated.  Use ros_openvino-msg:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:y-val is deprecated.  Use ros_openvino-msg:y instead.")
  (y m))

(cl:ensure-generic-function 'z-val :lambda-list '(m))
(cl:defmethod z-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:z-val is deprecated.  Use ros_openvino-msg:z instead.")
  (z m))

(cl:ensure-generic-function 'width-val :lambda-list '(m))
(cl:defmethod width-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:width-val is deprecated.  Use ros_openvino-msg:width instead.")
  (width m))

(cl:ensure-generic-function 'height-val :lambda-list '(m))
(cl:defmethod height-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:height-val is deprecated.  Use ros_openvino-msg:height instead.")
  (height m))

(cl:ensure-generic-function 'depth-val :lambda-list '(m))
(cl:defmethod depth-val ((m <ObjectBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:depth-val is deprecated.  Use ros_openvino-msg:depth instead.")
  (depth m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ObjectBox>) ostream)
  "Serializes a message object of type '<ObjectBox>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'label))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'label))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cls_id)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'confidence))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'width))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'height))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'depth))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ObjectBox>) istream)
  "Deserializes a message object of type '<ObjectBox>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'label) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'label) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cls_id)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'confidence) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'z) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'width) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'height) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'depth) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ObjectBox>)))
  "Returns string type for a message object of type '<ObjectBox>"
  "ros_openvino/ObjectBox")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ObjectBox)))
  "Returns string type for a message object of type 'ObjectBox"
  "ros_openvino/ObjectBox")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ObjectBox>)))
  "Returns md5sum for a message object of type '<ObjectBox>"
  "fa99cffdca4ba0958f353c904950d0fd")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ObjectBox)))
  "Returns md5sum for a message object of type 'ObjectBox"
  "fa99cffdca4ba0958f353c904950d0fd")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ObjectBox>)))
  "Returns full string definition for message of type '<ObjectBox>"
  (cl:format cl:nil "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for one 3D object detection result~%~%#which object is detected~%string label~%~%uint8 cls_id~%~%#confidence of result~%float32 confidence~%~%#box center of mass -> x in meters referred to frame_id parameter~%float32 x~%~%#box center of mass -> y in meters referred to frame_id parameter~%float32 y~%~%#box center of mass -> z in meters referred to frame_id parameter~%float32 z~%~%#box size -> width in meters~%float32 width~%~%#box size -> height in meters~%float32 height~%~%#box size ->depth in meters~%float32 depth~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ObjectBox)))
  "Returns full string definition for message of type 'ObjectBox"
  (cl:format cl:nil "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for one 3D object detection result~%~%#which object is detected~%string label~%~%uint8 cls_id~%~%#confidence of result~%float32 confidence~%~%#box center of mass -> x in meters referred to frame_id parameter~%float32 x~%~%#box center of mass -> y in meters referred to frame_id parameter~%float32 y~%~%#box center of mass -> z in meters referred to frame_id parameter~%float32 z~%~%#box size -> width in meters~%float32 width~%~%#box size -> height in meters~%float32 height~%~%#box size ->depth in meters~%float32 depth~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ObjectBox>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'label))
     1
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ObjectBox>))
  "Converts a ROS message object to a list"
  (cl:list 'ObjectBox
    (cl:cons ':label (label msg))
    (cl:cons ':cls_id (cls_id msg))
    (cl:cons ':confidence (confidence msg))
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
    (cl:cons ':z (z msg))
    (cl:cons ':width (width msg))
    (cl:cons ':height (height msg))
    (cl:cons ':depth (depth msg))
))
