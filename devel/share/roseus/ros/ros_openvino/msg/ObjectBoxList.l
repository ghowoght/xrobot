;; Auto-generated. Do not edit!


(when (boundp 'ros_openvino::ObjectBoxList)
  (if (not (find-package "ROS_OPENVINO"))
    (make-package "ROS_OPENVINO"))
  (shadow 'ObjectBoxList (find-package "ROS_OPENVINO")))
(unless (find-package "ROS_OPENVINO::OBJECTBOXLIST")
  (make-package "ROS_OPENVINO::OBJECTBOXLIST"))

(in-package "ROS")
;;//! \htmlinclude ObjectBoxList.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass ros_openvino::ObjectBoxList
  :super ros::object
  :slots (_header _objectboxes ))

(defmethod ros_openvino::ObjectBoxList
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:objectboxes __objectboxes) (let (r) (dotimes (i 0) (push (instance ros_openvino::ObjectBox :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _objectboxes __objectboxes)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:objectboxes
   (&rest __objectboxes)
   (if (keywordp (car __objectboxes))
       (send* _objectboxes __objectboxes)
     (progn
       (if __objectboxes (setq _objectboxes (car __objectboxes)))
       _objectboxes)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; ros_openvino/ObjectBox[] _objectboxes
    (apply #'+ (send-all _objectboxes :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; ros_openvino/ObjectBox[] _objectboxes
     (write-long (length _objectboxes) s)
     (dolist (elem _objectboxes)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; ros_openvino/ObjectBox[] _objectboxes
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _objectboxes (let (r) (dotimes (i n) (push (instance ros_openvino::ObjectBox :init) r)) r))
     (dolist (elem- _objectboxes)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get ros_openvino::ObjectBoxList :md5sum-) "b7e0c7f038f9303584d3c05093036eb0")
(setf (get ros_openvino::ObjectBoxList :datatype-) "ros_openvino/ObjectBoxList")
(setf (get ros_openvino::ObjectBoxList :definition-)
      "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).
#Copyright (c) 2019 Giovanni di Dio Bruno.
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
# 
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#This message is for listing 3D object detection results

#Standard ROS header
Header header

#Array of ObjectBox msgs, see more in ObjectBox.msg
ObjectBox[] objectboxes
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: ros_openvino/ObjectBox
#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).
#Copyright (c) 2019 Giovanni di Dio Bruno.
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
# 
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#This message is for one 3D object detection result

#which object is detected
string label

uint8 cls_id

#confidence of result
float32 confidence

#box center of mass -> x in meters referred to frame_id parameter
float32 x

#box center of mass -> y in meters referred to frame_id parameter
float32 y

#box center of mass -> z in meters referred to frame_id parameter
float32 z

#box size -> width in meters
float32 width

#box size -> height in meters
float32 height

#box size ->depth in meters
float32 depth
")



(provide :ros_openvino/ObjectBoxList "b7e0c7f038f9303584d3c05093036eb0")


