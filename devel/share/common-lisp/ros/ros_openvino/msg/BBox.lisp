; Auto-generated. Do not edit!


(cl:in-package ros_openvino-msg)


;//! \htmlinclude BBox.msg.html

(cl:defclass <BBox> (roslisp-msg-protocol:ros-message)
  ((label
    :reader label
    :initarg :label
    :type cl:string
    :initform "")
   (cls_id
    :reader cls_id
    :initarg :cls_id
    :type cl:fixnum
    :initform 0)
   (cls_name
    :reader cls_name
    :initarg :cls_name
    :type cl:string
    :initform "")
   (top_x
    :reader top_x
    :initarg :top_x
    :type cl:fixnum
    :initform 0)
   (top_y
    :reader top_y
    :initarg :top_y
    :type cl:fixnum
    :initform 0)
   (width
    :reader width
    :initarg :width
    :type cl:fixnum
    :initform 0)
   (height
    :reader height
    :initarg :height
    :type cl:fixnum
    :initform 0)
   (prob
    :reader prob
    :initarg :prob
    :type cl:float
    :initform 0.0))
)

(cl:defclass BBox (<BBox>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BBox>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BBox)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_openvino-msg:<BBox> is deprecated: use ros_openvino-msg:BBox instead.")))

(cl:ensure-generic-function 'label-val :lambda-list '(m))
(cl:defmethod label-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:label-val is deprecated.  Use ros_openvino-msg:label instead.")
  (label m))

(cl:ensure-generic-function 'cls_id-val :lambda-list '(m))
(cl:defmethod cls_id-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:cls_id-val is deprecated.  Use ros_openvino-msg:cls_id instead.")
  (cls_id m))

(cl:ensure-generic-function 'cls_name-val :lambda-list '(m))
(cl:defmethod cls_name-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:cls_name-val is deprecated.  Use ros_openvino-msg:cls_name instead.")
  (cls_name m))

(cl:ensure-generic-function 'top_x-val :lambda-list '(m))
(cl:defmethod top_x-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:top_x-val is deprecated.  Use ros_openvino-msg:top_x instead.")
  (top_x m))

(cl:ensure-generic-function 'top_y-val :lambda-list '(m))
(cl:defmethod top_y-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:top_y-val is deprecated.  Use ros_openvino-msg:top_y instead.")
  (top_y m))

(cl:ensure-generic-function 'width-val :lambda-list '(m))
(cl:defmethod width-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:width-val is deprecated.  Use ros_openvino-msg:width instead.")
  (width m))

(cl:ensure-generic-function 'height-val :lambda-list '(m))
(cl:defmethod height-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:height-val is deprecated.  Use ros_openvino-msg:height instead.")
  (height m))

(cl:ensure-generic-function 'prob-val :lambda-list '(m))
(cl:defmethod prob-val ((m <BBox>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:prob-val is deprecated.  Use ros_openvino-msg:prob instead.")
  (prob m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BBox>) ostream)
  "Serializes a message object of type '<BBox>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'label))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'label))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cls_id)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'cls_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'cls_name))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'top_x)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'top_x)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'top_y)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'top_y)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'width)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'width)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'height)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'height)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'prob))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BBox>) istream)
  "Deserializes a message object of type '<BBox>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'label) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'label) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cls_id)) (cl:read-byte istream))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cls_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'cls_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'top_x)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'top_x)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'top_y)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'top_y)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'width)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'width)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'height)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'height)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'prob) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BBox>)))
  "Returns string type for a message object of type '<BBox>"
  "ros_openvino/BBox")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BBox)))
  "Returns string type for a message object of type 'BBox"
  "ros_openvino/BBox")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BBox>)))
  "Returns md5sum for a message object of type '<BBox>"
  "ca04bfc116938fe901ae8d0c896cf4ab")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BBox)))
  "Returns md5sum for a message object of type 'BBox"
  "ca04bfc116938fe901ae8d0c896cf4ab")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BBox>)))
  "Returns full string definition for message of type '<BBox>"
  (cl:format cl:nil "########################################~%# Message for defining bounding box used in object detection result~%#~%# Created on 2018-12-12~%########################################~%~%string label~%uint8 cls_id~%string cls_name~%uint16 top_x~%uint16 top_y~%uint16 width~%uint16 height~%float32 prob    # OD probability~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BBox)))
  "Returns full string definition for message of type 'BBox"
  (cl:format cl:nil "########################################~%# Message for defining bounding box used in object detection result~%#~%# Created on 2018-12-12~%########################################~%~%string label~%uint8 cls_id~%string cls_name~%uint16 top_x~%uint16 top_y~%uint16 width~%uint16 height~%float32 prob    # OD probability~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BBox>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'label))
     1
     4 (cl:length (cl:slot-value msg 'cls_name))
     2
     2
     2
     2
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BBox>))
  "Converts a ROS message object to a list"
  (cl:list 'BBox
    (cl:cons ':label (label msg))
    (cl:cons ':cls_id (cls_id msg))
    (cl:cons ':cls_name (cls_name msg))
    (cl:cons ':top_x (top_x msg))
    (cl:cons ':top_y (top_y msg))
    (cl:cons ':width (width msg))
    (cl:cons ':height (height msg))
    (cl:cons ':prob (prob msg))
))
