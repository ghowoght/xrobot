;; Auto-generated. Do not edit!


(when (boundp 'ros_openvino::ObjectAttr)
  (if (not (find-package "ROS_OPENVINO"))
    (make-package "ROS_OPENVINO"))
  (shadow 'ObjectAttr (find-package "ROS_OPENVINO")))
(unless (find-package "ROS_OPENVINO::OBJECTATTR")
  (make-package "ROS_OPENVINO::OBJECTATTR"))

(in-package "ROS")
;;//! \htmlinclude ObjectAttr.msg.html


(defclass ros_openvino::ObjectAttr
  :super ros::object
  :slots (_color _area _area_label ))

(defmethod ros_openvino::ObjectAttr
  (:init
   (&key
    ((:color __color) "")
    ((:area __area) 0.0)
    ((:area_label __area_label) "")
    )
   (send-super :init)
   (setq _color (string __color))
   (setq _area (float __area))
   (setq _area_label (string __area_label))
   self)
  (:color
   (&optional __color)
   (if __color (setq _color __color)) _color)
  (:area
   (&optional __area)
   (if __area (setq _area __area)) _area)
  (:area_label
   (&optional __area_label)
   (if __area_label (setq _area_label __area_label)) _area_label)
  (:serialization-length
   ()
   (+
    ;; string _color
    4 (length _color)
    ;; float32 _area
    4
    ;; string _area_label
    4 (length _area_label)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _color
       (write-long (length _color) s) (princ _color s)
     ;; float32 _area
       (sys::poke _area (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; string _area_label
       (write-long (length _area_label) s) (princ _area_label s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _color
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _color (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _area
     (setq _area (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; string _area_label
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _area_label (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get ros_openvino::ObjectAttr :md5sum-) "24bd612f87ca0e13008c32a7663fc03d")
(setf (get ros_openvino::ObjectAttr :datatype-) "ros_openvino/ObjectAttr")
(setf (get ros_openvino::ObjectAttr :definition-)
      "########################################
# Message for non-person object attributes
#
# Created on 2019-09-17
########################################

string color
float32 area
string area_label   # small, middle, or large

")



(provide :ros_openvino/ObjectAttr "24bd612f87ca0e13008c32a7663fc03d")


