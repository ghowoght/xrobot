; Auto-generated. Do not edit!


(cl:in-package ros_openvino-msg)


;//! \htmlinclude ObjectAttr.msg.html

(cl:defclass <ObjectAttr> (roslisp-msg-protocol:ros-message)
  ((color
    :reader color
    :initarg :color
    :type cl:string
    :initform "")
   (area
    :reader area
    :initarg :area
    :type cl:float
    :initform 0.0)
   (area_label
    :reader area_label
    :initarg :area_label
    :type cl:string
    :initform ""))
)

(cl:defclass ObjectAttr (<ObjectAttr>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ObjectAttr>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ObjectAttr)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_openvino-msg:<ObjectAttr> is deprecated: use ros_openvino-msg:ObjectAttr instead.")))

(cl:ensure-generic-function 'color-val :lambda-list '(m))
(cl:defmethod color-val ((m <ObjectAttr>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:color-val is deprecated.  Use ros_openvino-msg:color instead.")
  (color m))

(cl:ensure-generic-function 'area-val :lambda-list '(m))
(cl:defmethod area-val ((m <ObjectAttr>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:area-val is deprecated.  Use ros_openvino-msg:area instead.")
  (area m))

(cl:ensure-generic-function 'area_label-val :lambda-list '(m))
(cl:defmethod area_label-val ((m <ObjectAttr>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:area_label-val is deprecated.  Use ros_openvino-msg:area_label instead.")
  (area_label m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ObjectAttr>) ostream)
  "Serializes a message object of type '<ObjectAttr>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'color))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'color))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'area))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'area_label))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'area_label))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ObjectAttr>) istream)
  "Deserializes a message object of type '<ObjectAttr>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'color) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'color) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'area) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'area_label) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'area_label) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ObjectAttr>)))
  "Returns string type for a message object of type '<ObjectAttr>"
  "ros_openvino/ObjectAttr")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ObjectAttr)))
  "Returns string type for a message object of type 'ObjectAttr"
  "ros_openvino/ObjectAttr")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ObjectAttr>)))
  "Returns md5sum for a message object of type '<ObjectAttr>"
  "24bd612f87ca0e13008c32a7663fc03d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ObjectAttr)))
  "Returns md5sum for a message object of type 'ObjectAttr"
  "24bd612f87ca0e13008c32a7663fc03d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ObjectAttr>)))
  "Returns full string definition for message of type '<ObjectAttr>"
  (cl:format cl:nil "########################################~%# Message for non-person object attributes~%#~%# Created on 2019-09-17~%########################################~%~%string color~%float32 area~%string area_label   # small, middle, or large~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ObjectAttr)))
  "Returns full string definition for message of type 'ObjectAttr"
  (cl:format cl:nil "########################################~%# Message for non-person object attributes~%#~%# Created on 2019-09-17~%########################################~%~%string color~%float32 area~%string area_label   # small, middle, or large~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ObjectAttr>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'color))
     4
     4 (cl:length (cl:slot-value msg 'area_label))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ObjectAttr>))
  "Converts a ROS message object to a list"
  (cl:list 'ObjectAttr
    (cl:cons ':color (color msg))
    (cl:cons ':area (area msg))
    (cl:cons ':area_label (area_label msg))
))
