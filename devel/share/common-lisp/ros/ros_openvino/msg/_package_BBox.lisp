(cl:in-package ros_openvino-msg)
(cl:export '(LABEL-VAL
          LABEL
          CLS_ID-VAL
          CLS_ID
          CLS_NAME-VAL
          CLS_NAME
          TOP_X-VAL
          TOP_X
          TOP_Y-VAL
          TOP_Y
          WIDTH-VAL
          WIDTH
          HEIGHT-VAL
          HEIGHT
          PROB-VAL
          PROB
))