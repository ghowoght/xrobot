# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include;/usr/include".split(';') if "${prefix}/include;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "cv_bridge;roscpp;std_msgs;image_transport;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lyolo_ros_vino".split(';') if "-lyolo_ros_vino" != "" else []
PROJECT_NAME = "yolo_ros_vino"
PROJECT_SPACE_DIR = "/home/ghowoght/workplace/myrobot/install"
PROJECT_VERSION = "1.1.3"
