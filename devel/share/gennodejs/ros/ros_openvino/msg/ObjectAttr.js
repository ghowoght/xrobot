// Auto-generated. Do not edit!

// (in-package ros_openvino.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class ObjectAttr {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.color = null;
      this.area = null;
      this.area_label = null;
    }
    else {
      if (initObj.hasOwnProperty('color')) {
        this.color = initObj.color
      }
      else {
        this.color = '';
      }
      if (initObj.hasOwnProperty('area')) {
        this.area = initObj.area
      }
      else {
        this.area = 0.0;
      }
      if (initObj.hasOwnProperty('area_label')) {
        this.area_label = initObj.area_label
      }
      else {
        this.area_label = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ObjectAttr
    // Serialize message field [color]
    bufferOffset = _serializer.string(obj.color, buffer, bufferOffset);
    // Serialize message field [area]
    bufferOffset = _serializer.float32(obj.area, buffer, bufferOffset);
    // Serialize message field [area_label]
    bufferOffset = _serializer.string(obj.area_label, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ObjectAttr
    let len;
    let data = new ObjectAttr(null);
    // Deserialize message field [color]
    data.color = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [area]
    data.area = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [area_label]
    data.area_label = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.color.length;
    length += object.area_label.length;
    return length + 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'ros_openvino/ObjectAttr';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '24bd612f87ca0e13008c32a7663fc03d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    ########################################
    # Message for non-person object attributes
    #
    # Created on 2019-09-17
    ########################################
    
    string color
    float32 area
    string area_label   # small, middle, or large
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ObjectAttr(null);
    if (msg.color !== undefined) {
      resolved.color = msg.color;
    }
    else {
      resolved.color = ''
    }

    if (msg.area !== undefined) {
      resolved.area = msg.area;
    }
    else {
      resolved.area = 0.0
    }

    if (msg.area_label !== undefined) {
      resolved.area_label = msg.area_label;
    }
    else {
      resolved.area_label = ''
    }

    return resolved;
    }
};

module.exports = ObjectAttr;
