# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ghowoght/workplace/myrobot/src/yolo_ros_vino/src/yolo_ros_vino.cpp" "/home/ghowoght/workplace/myrobot/build/yolo_ros_vino/CMakeFiles/yolo_ros_vino.dir/src/yolo_ros_vino.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"yolo_ros_vino\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/ghowoght/workplace/myrobot/devel/include"
  "/usr/include/opencv"
  "/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/include"
  "/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/include/../samples/common"
  "/opt/intel/openvino_2020.4.287/deployment_tools/inference_engine/include/../src/extension"
  "/home/ghowoght/workplace/myrobot/src/yolo_ros_vino/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
