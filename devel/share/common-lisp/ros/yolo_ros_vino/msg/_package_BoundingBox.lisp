(cl:in-package yolo_ros_vino-msg)
(cl:export '(CLASS-VAL
          CLASS
          PROBABILITY-VAL
          PROBABILITY
          XMIN-VAL
          XMIN
          YMIN-VAL
          YMIN
          XMAX-VAL
          XMAX
          YMAX-VAL
          YMAX
))