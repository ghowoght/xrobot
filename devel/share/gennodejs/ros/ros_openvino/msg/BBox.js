// Auto-generated. Do not edit!

// (in-package ros_openvino.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class BBox {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.label = null;
      this.cls_id = null;
      this.cls_name = null;
      this.top_x = null;
      this.top_y = null;
      this.width = null;
      this.height = null;
      this.prob = null;
    }
    else {
      if (initObj.hasOwnProperty('label')) {
        this.label = initObj.label
      }
      else {
        this.label = '';
      }
      if (initObj.hasOwnProperty('cls_id')) {
        this.cls_id = initObj.cls_id
      }
      else {
        this.cls_id = 0;
      }
      if (initObj.hasOwnProperty('cls_name')) {
        this.cls_name = initObj.cls_name
      }
      else {
        this.cls_name = '';
      }
      if (initObj.hasOwnProperty('top_x')) {
        this.top_x = initObj.top_x
      }
      else {
        this.top_x = 0;
      }
      if (initObj.hasOwnProperty('top_y')) {
        this.top_y = initObj.top_y
      }
      else {
        this.top_y = 0;
      }
      if (initObj.hasOwnProperty('width')) {
        this.width = initObj.width
      }
      else {
        this.width = 0;
      }
      if (initObj.hasOwnProperty('height')) {
        this.height = initObj.height
      }
      else {
        this.height = 0;
      }
      if (initObj.hasOwnProperty('prob')) {
        this.prob = initObj.prob
      }
      else {
        this.prob = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type BBox
    // Serialize message field [label]
    bufferOffset = _serializer.string(obj.label, buffer, bufferOffset);
    // Serialize message field [cls_id]
    bufferOffset = _serializer.uint8(obj.cls_id, buffer, bufferOffset);
    // Serialize message field [cls_name]
    bufferOffset = _serializer.string(obj.cls_name, buffer, bufferOffset);
    // Serialize message field [top_x]
    bufferOffset = _serializer.uint16(obj.top_x, buffer, bufferOffset);
    // Serialize message field [top_y]
    bufferOffset = _serializer.uint16(obj.top_y, buffer, bufferOffset);
    // Serialize message field [width]
    bufferOffset = _serializer.uint16(obj.width, buffer, bufferOffset);
    // Serialize message field [height]
    bufferOffset = _serializer.uint16(obj.height, buffer, bufferOffset);
    // Serialize message field [prob]
    bufferOffset = _serializer.float32(obj.prob, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type BBox
    let len;
    let data = new BBox(null);
    // Deserialize message field [label]
    data.label = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [cls_id]
    data.cls_id = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [cls_name]
    data.cls_name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [top_x]
    data.top_x = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [top_y]
    data.top_y = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [width]
    data.width = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [height]
    data.height = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [prob]
    data.prob = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.label.length;
    length += object.cls_name.length;
    return length + 21;
  }

  static datatype() {
    // Returns string type for a message object
    return 'ros_openvino/BBox';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ca04bfc116938fe901ae8d0c896cf4ab';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    ########################################
    # Message for defining bounding box used in object detection result
    #
    # Created on 2018-12-12
    ########################################
    
    string label
    uint8 cls_id
    string cls_name
    uint16 top_x
    uint16 top_y
    uint16 width
    uint16 height
    float32 prob    # OD probability
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new BBox(null);
    if (msg.label !== undefined) {
      resolved.label = msg.label;
    }
    else {
      resolved.label = ''
    }

    if (msg.cls_id !== undefined) {
      resolved.cls_id = msg.cls_id;
    }
    else {
      resolved.cls_id = 0
    }

    if (msg.cls_name !== undefined) {
      resolved.cls_name = msg.cls_name;
    }
    else {
      resolved.cls_name = ''
    }

    if (msg.top_x !== undefined) {
      resolved.top_x = msg.top_x;
    }
    else {
      resolved.top_x = 0
    }

    if (msg.top_y !== undefined) {
      resolved.top_y = msg.top_y;
    }
    else {
      resolved.top_y = 0
    }

    if (msg.width !== undefined) {
      resolved.width = msg.width;
    }
    else {
      resolved.width = 0
    }

    if (msg.height !== undefined) {
      resolved.height = msg.height;
    }
    else {
      resolved.height = 0
    }

    if (msg.prob !== undefined) {
      resolved.prob = msg.prob;
    }
    else {
      resolved.prob = 0.0
    }

    return resolved;
    }
};

module.exports = BBox;
