; Auto-generated. Do not edit!


(cl:in-package ros_openvino-msg)


;//! \htmlinclude Objects.msg.html

(cl:defclass <Objects> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (objects
    :reader objects
    :initarg :objects
    :type (cl:vector ros_openvino-msg:Object)
   :initform (cl:make-array 0 :element-type 'ros_openvino-msg:Object :initial-element (cl:make-instance 'ros_openvino-msg:Object))))
)

(cl:defclass Objects (<Objects>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Objects>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Objects)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_openvino-msg:<Objects> is deprecated: use ros_openvino-msg:Objects instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Objects>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:header-val is deprecated.  Use ros_openvino-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'objects-val :lambda-list '(m))
(cl:defmethod objects-val ((m <Objects>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:objects-val is deprecated.  Use ros_openvino-msg:objects instead.")
  (objects m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Objects>) ostream)
  "Serializes a message object of type '<Objects>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'objects))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'objects))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Objects>) istream)
  "Deserializes a message object of type '<Objects>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'objects) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'objects)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ros_openvino-msg:Object))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Objects>)))
  "Returns string type for a message object of type '<Objects>"
  "ros_openvino/Objects")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Objects)))
  "Returns string type for a message object of type 'Objects"
  "ros_openvino/Objects")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Objects>)))
  "Returns md5sum for a message object of type '<Objects>"
  "3bce31b9ee9caa513d868c04b59d4cb2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Objects)))
  "Returns md5sum for a message object of type 'Objects"
  "3bce31b9ee9caa513d868c04b59d4cb2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Objects>)))
  "Returns full string definition for message of type '<Objects>"
  (cl:format cl:nil "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for listing 2D object detection results~%~%#Standard header~%Header header~%~%#Array of Object, see more in Object.msg~%Object[] objects~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: ros_openvino/Object~%#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for one 2D object detection result~%~%#which object is detected~%string label~%~%#confidence of result~%float32 confidence~%~%#normalized value of box top-left point coordinate x~%float32 x~%~%#normalized value of box top-left point coordinate y~%float32 y~%~%#normalized value of box height~%float32 height~%~%#normalized value of box width~%float32 width~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Objects)))
  "Returns full string definition for message of type 'Objects"
  (cl:format cl:nil "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for listing 2D object detection results~%~%#Standard header~%Header header~%~%#Array of Object, see more in Object.msg~%Object[] objects~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: ros_openvino/Object~%#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for one 2D object detection result~%~%#which object is detected~%string label~%~%#confidence of result~%float32 confidence~%~%#normalized value of box top-left point coordinate x~%float32 x~%~%#normalized value of box top-left point coordinate y~%float32 y~%~%#normalized value of box height~%float32 height~%~%#normalized value of box width~%float32 width~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Objects>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'objects) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Objects>))
  "Converts a ROS message object to a list"
  (cl:list 'Objects
    (cl:cons ':header (header msg))
    (cl:cons ':objects (objects msg))
))
