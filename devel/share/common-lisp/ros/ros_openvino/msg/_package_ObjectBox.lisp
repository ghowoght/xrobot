(cl:in-package ros_openvino-msg)
(cl:export '(LABEL-VAL
          LABEL
          CLS_ID-VAL
          CLS_ID
          CONFIDENCE-VAL
          CONFIDENCE
          X-VAL
          X
          Y-VAL
          Y
          Z-VAL
          Z
          WIDTH-VAL
          WIDTH
          HEIGHT-VAL
          HEIGHT
          DEPTH-VAL
          DEPTH
))