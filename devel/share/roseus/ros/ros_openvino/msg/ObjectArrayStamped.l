;; Auto-generated. Do not edit!


(when (boundp 'ros_openvino::ObjectArrayStamped)
  (if (not (find-package "ROS_OPENVINO"))
    (make-package "ROS_OPENVINO"))
  (shadow 'ObjectArrayStamped (find-package "ROS_OPENVINO")))
(unless (find-package "ROS_OPENVINO::OBJECTARRAYSTAMPED")
  (make-package "ROS_OPENVINO::OBJECTARRAYSTAMPED"))

(in-package "ROS")
;;//! \htmlinclude ObjectArrayStamped.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass ros_openvino::ObjectArrayStamped
  :super ros::object
  :slots (_header _data ))

(defmethod ros_openvino::ObjectArrayStamped
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:data __data) (let (r) (dotimes (i 0) (push (instance ros_openvino::Object :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _data __data)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:data
   (&rest __data)
   (if (keywordp (car __data))
       (send* _data __data)
     (progn
       (if __data (setq _data (car __data)))
       _data)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; ros_openvino/Object[] _data
    (apply #'+ (send-all _data :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; ros_openvino/Object[] _data
     (write-long (length _data) s)
     (dolist (elem _data)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; ros_openvino/Object[] _data
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _data (let (r) (dotimes (i n) (push (instance ros_openvino::Object :init) r)) r))
     (dolist (elem- _data)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get ros_openvino::ObjectArrayStamped :md5sum-) "330c05cf1463f0666552e4359e9fcb3b")
(setf (get ros_openvino::ObjectArrayStamped :datatype-) "ros_openvino/ObjectArrayStamped")
(setf (get ros_openvino::ObjectArrayStamped :definition-)
      "########################################
# Message for object array used to save object (include person) detection result
#
# Created on 2019-09-17
########################################

std_msgs/Header header
Object[] data

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: ros_openvino/Object
########################################
# Message for single object detected
#
# Created on 2019-09-17
########################################

BBox bbox
ObjectAttr obj_attr

================================================================================
MSG: ros_openvino/BBox
########################################
# Message for defining bounding box used in object detection result
#
# Created on 2018-12-12
########################################

string label
uint8 cls_id
string cls_name
uint16 top_x
uint16 top_y
uint16 width
uint16 height
float32 prob    # OD probability

================================================================================
MSG: ros_openvino/ObjectAttr
########################################
# Message for non-person object attributes
#
# Created on 2019-09-17
########################################

string color
float32 area
string area_label   # small, middle, or large

")



(provide :ros_openvino/ObjectArrayStamped "330c05cf1463f0666552e4359e9fcb3b")


