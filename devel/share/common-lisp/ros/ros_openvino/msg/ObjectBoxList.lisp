; Auto-generated. Do not edit!


(cl:in-package ros_openvino-msg)


;//! \htmlinclude ObjectBoxList.msg.html

(cl:defclass <ObjectBoxList> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (objectboxes
    :reader objectboxes
    :initarg :objectboxes
    :type (cl:vector ros_openvino-msg:ObjectBox)
   :initform (cl:make-array 0 :element-type 'ros_openvino-msg:ObjectBox :initial-element (cl:make-instance 'ros_openvino-msg:ObjectBox))))
)

(cl:defclass ObjectBoxList (<ObjectBoxList>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ObjectBoxList>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ObjectBoxList)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_openvino-msg:<ObjectBoxList> is deprecated: use ros_openvino-msg:ObjectBoxList instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ObjectBoxList>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:header-val is deprecated.  Use ros_openvino-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'objectboxes-val :lambda-list '(m))
(cl:defmethod objectboxes-val ((m <ObjectBoxList>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_openvino-msg:objectboxes-val is deprecated.  Use ros_openvino-msg:objectboxes instead.")
  (objectboxes m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ObjectBoxList>) ostream)
  "Serializes a message object of type '<ObjectBoxList>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'objectboxes))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'objectboxes))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ObjectBoxList>) istream)
  "Deserializes a message object of type '<ObjectBoxList>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'objectboxes) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'objectboxes)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ros_openvino-msg:ObjectBox))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ObjectBoxList>)))
  "Returns string type for a message object of type '<ObjectBoxList>"
  "ros_openvino/ObjectBoxList")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ObjectBoxList)))
  "Returns string type for a message object of type 'ObjectBoxList"
  "ros_openvino/ObjectBoxList")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ObjectBoxList>)))
  "Returns md5sum for a message object of type '<ObjectBoxList>"
  "b7e0c7f038f9303584d3c05093036eb0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ObjectBoxList)))
  "Returns md5sum for a message object of type 'ObjectBoxList"
  "b7e0c7f038f9303584d3c05093036eb0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ObjectBoxList>)))
  "Returns full string definition for message of type '<ObjectBoxList>"
  (cl:format cl:nil "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for listing 3D object detection results~%~%#Standard ROS header~%Header header~%~%#Array of ObjectBox msgs, see more in ObjectBox.msg~%ObjectBox[] objectboxes~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: ros_openvino/ObjectBox~%#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for one 3D object detection result~%~%#which object is detected~%string label~%~%uint8 cls_id~%~%#confidence of result~%float32 confidence~%~%#box center of mass -> x in meters referred to frame_id parameter~%float32 x~%~%#box center of mass -> y in meters referred to frame_id parameter~%float32 y~%~%#box center of mass -> z in meters referred to frame_id parameter~%float32 z~%~%#box size -> width in meters~%float32 width~%~%#box size -> height in meters~%float32 height~%~%#box size ->depth in meters~%float32 depth~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ObjectBoxList)))
  "Returns full string definition for message of type 'ObjectBoxList"
  (cl:format cl:nil "#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for listing 3D object detection results~%~%#Standard ROS header~%Header header~%~%#Array of ObjectBox msgs, see more in ObjectBox.msg~%ObjectBox[] objectboxes~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: ros_openvino/ObjectBox~%#This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).~%#Copyright (c) 2019 Giovanni di Dio Bruno.~%#~%#This program is free software: you can redistribute it and/or modify~%#it under the terms of the GNU Affero General Public License as published by~%#the Free Software Foundation, either version 3 of the License, or~%#(at your option) any later version.~%#~%#This program is distributed in the hope that it will be useful,~%#but WITHOUT ANY WARRANTY; without even the implied warranty of~%#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the~%#GNU Affero General Public License for more details.~%# ~%#You should have received a copy of the GNU Affero General Public License~%#along with this program. If not, see <http://www.gnu.org/licenses/>.~%~%#This message is for one 3D object detection result~%~%#which object is detected~%string label~%~%uint8 cls_id~%~%#confidence of result~%float32 confidence~%~%#box center of mass -> x in meters referred to frame_id parameter~%float32 x~%~%#box center of mass -> y in meters referred to frame_id parameter~%float32 y~%~%#box center of mass -> z in meters referred to frame_id parameter~%float32 z~%~%#box size -> width in meters~%float32 width~%~%#box size -> height in meters~%float32 height~%~%#box size ->depth in meters~%float32 depth~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ObjectBoxList>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'objectboxes) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ObjectBoxList>))
  "Converts a ROS message object to a list"
  (cl:list 'ObjectBoxList
    (cl:cons ':header (header msg))
    (cl:cons ':objectboxes (objectboxes msg))
))
