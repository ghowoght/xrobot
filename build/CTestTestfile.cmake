# CMake generated Testfile for 
# Source directory: /home/ghowoght/workplace/myrobot/src
# Build directory: /home/ghowoght/workplace/myrobot/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("remote_ctrl_node")
subdirs("semantic_orb_slam2_ros")
subdirs("serial_node")
subdirs("depthimage_to_laserscan")
subdirs("ros_openvino")
subdirs("rplidar_ros")
subdirs("m-explore/map_merge")
subdirs("m-explore/explore")
subdirs("xrobot")
