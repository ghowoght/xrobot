// Auto-generated. Do not edit!

// (in-package ros_openvino.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let ObjectBox = require('./ObjectBox.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class ObjectBoxList {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.objectboxes = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('objectboxes')) {
        this.objectboxes = initObj.objectboxes
      }
      else {
        this.objectboxes = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ObjectBoxList
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [objectboxes]
    // Serialize the length for message field [objectboxes]
    bufferOffset = _serializer.uint32(obj.objectboxes.length, buffer, bufferOffset);
    obj.objectboxes.forEach((val) => {
      bufferOffset = ObjectBox.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ObjectBoxList
    let len;
    let data = new ObjectBoxList(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [objectboxes]
    // Deserialize array length for message field [objectboxes]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.objectboxes = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.objectboxes[i] = ObjectBox.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    object.objectboxes.forEach((val) => {
      length += ObjectBox.getMessageSize(val);
    });
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'ros_openvino/ObjectBoxList';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b7e0c7f038f9303584d3c05093036eb0';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).
    #Copyright (c) 2019 Giovanni di Dio Bruno.
    #
    #This program is free software: you can redistribute it and/or modify
    #it under the terms of the GNU Affero General Public License as published by
    #the Free Software Foundation, either version 3 of the License, or
    #(at your option) any later version.
    #
    #This program is distributed in the hope that it will be useful,
    #but WITHOUT ANY WARRANTY; without even the implied warranty of
    #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #GNU Affero General Public License for more details.
    # 
    #You should have received a copy of the GNU Affero General Public License
    #along with this program. If not, see <http://www.gnu.org/licenses/>.
    
    #This message is for listing 3D object detection results
    
    #Standard ROS header
    Header header
    
    #Array of ObjectBox msgs, see more in ObjectBox.msg
    ObjectBox[] objectboxes
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: ros_openvino/ObjectBox
    #This file is part of the ros_openvino package (https://github.com/gbr1/ros_openvino or http://gbr1.github.io).
    #Copyright (c) 2019 Giovanni di Dio Bruno.
    #
    #This program is free software: you can redistribute it and/or modify
    #it under the terms of the GNU Affero General Public License as published by
    #the Free Software Foundation, either version 3 of the License, or
    #(at your option) any later version.
    #
    #This program is distributed in the hope that it will be useful,
    #but WITHOUT ANY WARRANTY; without even the implied warranty of
    #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #GNU Affero General Public License for more details.
    # 
    #You should have received a copy of the GNU Affero General Public License
    #along with this program. If not, see <http://www.gnu.org/licenses/>.
    
    #This message is for one 3D object detection result
    
    #which object is detected
    string label
    
    uint8 cls_id
    
    #confidence of result
    float32 confidence
    
    #box center of mass -> x in meters referred to frame_id parameter
    float32 x
    
    #box center of mass -> y in meters referred to frame_id parameter
    float32 y
    
    #box center of mass -> z in meters referred to frame_id parameter
    float32 z
    
    #box size -> width in meters
    float32 width
    
    #box size -> height in meters
    float32 height
    
    #box size ->depth in meters
    float32 depth
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ObjectBoxList(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.objectboxes !== undefined) {
      resolved.objectboxes = new Array(msg.objectboxes.length);
      for (let i = 0; i < resolved.objectboxes.length; ++i) {
        resolved.objectboxes[i] = ObjectBox.Resolve(msg.objectboxes[i]);
      }
    }
    else {
      resolved.objectboxes = []
    }

    return resolved;
    }
};

module.exports = ObjectBoxList;
